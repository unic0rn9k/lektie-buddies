use crate::{
    config,
    import::*,
    USER_HASH,
    unwrap,
    anyhow,
    redirect,
    SESSION_ID,
};

fn token() -> String{
    rand::thread_rng()
            .sample_iter(&Alphanumeric)
            .take(20)
            .collect()
}

#[derive(Deserialize, Serialize, Debug)]
pub struct UserAuth {
    password: String,
    username: String,
    password2: Option<String>,
}

impl UserAuth{
    pub fn auth(&self) -> Result<()>{
        let hash = unwrap!(
            Some(unwrap!(
                Ok(USER_HASH.get(&self.username)),
                "Unable to open USER_HASH"
            )?),
            "No such user"
        )?;
        if !unwrap!(Ok(verify(&self.password, &String::from_utf8_lossy(&hash[..]))), "BCRYPT ERROR")?{
            return anyhow!("Wrong password")
        }

        Ok(())
    }
}

#[macro_export]
macro_rules! anyhow{
    ($($t:tt)*) => {{
        Err(error::ErrorBadRequest(format!($($t)*)))
    }}
}

#[macro_export]
macro_rules! unwrap{
    ($ok: tt($match: expr), $b: expr) => {{
        match $match{
            $ok(ok) => Ok(ok),
            err => anyhow!("Error in: {}\n    {}", stringify!($match), format!("{}: {:?}", $b, err).replace("\n","\n    ")),
        }
    }}
}

pub async fn signup(form: web::Form<UserAuth>) -> Result<HttpResponse>{
    if form.password != form.password2.clone().unwrap(){
        return anyhow!("Passwords do not match")
    }
    println!(" => {} : {:?}", "auth new user".bold().yellow(), form);
    if unwrap!(Ok(USER_HASH.get(&form.username)), "Unable to open USER_HASH")?.is_some(){
        return anyhow!("User exists")
    }

    unwrap!(Ok(USER_HASH.insert(form.username.clone(), &unwrap!(Ok(hash(form.password.clone(), config::HASH_COST)),"HASH") ?[..])), "")?;
    Ok(redirect("/login/index.html"))
}

pub async fn login(session: Session, form: web::Form<UserAuth>) -> Result<HttpResponse>{
    form.auth()?;

    let mut token_ = token();
    while unwrap!(Ok(SESSION_ID.get(token_.clone())), "ERROR CHECKING SESSION TOKEN")?.is_some(){
        token_ = token();
    }

    unwrap!(Ok(SESSION_ID.insert(token_.clone(), &form.username[..])), "ERROR SAVING SESSION TOKEN TO DB")?;
    session.set("ID", token_)?;
    Ok(redirect("/"))
}

pub async fn logout(session: Session) -> Result<HttpResponse>{
    unwrap!(Ok(SESSION_ID.remove(
            &unwrap!(Some(session.get::<String>("ID").unwrap_or(None)), "ERROR: are you logged in?" )? )
        ),
        "ERROR REMOVING TOKEN FROM DB"
    )?;
    session.remove("ID");
    Ok(redirect("/"))
}
