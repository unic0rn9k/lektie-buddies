mod config;
mod auth;
mod import;
mod user;
mod chat;

use import::*;

fn parse_html(path: &str, data: HashMap<&str, &str>) -> Result<String>{
    let mut buffer = "".to_string();
    File::open(path)?.read_to_string(&mut buffer)?;
    let variable = Regex::new(r"\{\{(?P<var>[a-zA-Z0-9_]*)\}\}").unwrap();

    for cap in variable.captures_iter(&buffer.clone()){
        let var = &cap["var"];
        buffer = buffer.replace(&format!("{{{{{var}}}}}", var=var), match data.get(var){
            Some(ok) => ok,
            None => return anyhow!("variable '{}' not found. Error in {}", var, path)
        })
    }

    Ok(buffer)
}


pub fn redirect(url: &str) -> HttpResponse{
    HttpResponse::Ok()
        .content_type("text/html")
        .body( format!(r#"<meta http-equiv="refresh" content="0; url={}" />"#, url) )
}

lazy_static!{
    pub static ref USER_HASH     : sled::Db = sled::open("USER_HASH").unwrap();
    pub static ref SESSION_ID    : sled::Db = sled::open("SESSION_DB").unwrap();
    pub static ref USER_DATA     : sled::Db = sled::open("USER_DATA").unwrap();
    pub static ref USER_MATCHES  : sled::Db = sled::open("USER_MATCHES").unwrap();
    pub static ref CONVERSATIONS : sled::Db = sled::open("CONVERSATIONS").unwrap();
}

async fn start(session: Session) -> Result<HttpResponse> {
    println!("START");
    match session.get::<String>("ID")?{
        Some(session_id) => user::index(&session_id),
        None => Ok(redirect("/login/index.html")),
    }
}

async fn index(req: HttpRequest) -> Result<NamedFile> {
    println!("INDEX");
    let path: PathBuf = format!("web/{}", req.match_info().query("filename")).parse().unwrap();
    Ok(NamedFile::open(path)?)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    use actix_web::{web, App, HttpServer};

    println!("lektie buddies running on http://{}", config::ADDR.bold().green());

    HttpServer::new(
        || App::new().wrap(CookieSession::signed(&[0; 32]).secure(false))

                .service(web::resource("/").route(web::get().to(start)))
                .service(web::resource("/home").route(web::get().to(start)))

                .service(web::resource("/match/{username:.*}/{redirect:.*}").route(web::get().to(user::match_users)))
                .service(web::resource("/dont_match/{username:.*}").route(web::get().to(user::dont_match)))
                .service(web::resource("/contacts").route(web::get().to(user::contacts)))

                .service(web::resource("/profile"      ).route(web::get().to(user::profile)))
                .service(web::resource("/update_profile").route(web::post().to(user::update_user)))
                .service(web::resource("/login"        ).route(web::post().to(auth::login)))
                .service(web::resource("/logout"        ).route(web::get().to(auth::logout)))
                .service(web::resource("/signup"       ).route(web::post().to(auth::signup)))
                .service(web::resource("/{filename:.*}").route(web::get().to(index)))
        )
        .bind(config::ADDR)?
        .run()
        .await
}
