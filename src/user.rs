use crate::{
    import::*,
    anyhow,
    unwrap,
    redirect,
    parse_html,
    SESSION_ID,
    USER_DATA,
    USER_MATCHES,
};

use std::collections::HashMap;

macro_rules! str{
    ($($t:tt)*) => {{String::from_utf8_lossy(&$($t)*[..]).to_string()}}
}

macro_rules! mapify{
    ($src:expr, |$v:ident: $t:ty|{$do: expr} [$($field: ident),*] $(<$a:ty, $b:ty>)?) => {{
        let mut map = HashMap$(::<$a, $b>)?::new();
        let f = |$v: $t|{$do};
        $(
            map.insert(stringify!($field), f($src.$field));
        )*
        map
    }}
}
macro_rules! compare{
    ($srca:expr, $srcb:expr, |$a:ident, $b:ident : $t:ty|{$do: expr} [$($field: ident),*]) => {{
        let f = |$a: &$t, $b: &$t|{$do};
        [$(
            f(&$srca.$field, &$srcb.$field)
        ),*]
    }}
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct UserUpdate{
    bio: String,
    Man: Option<String>,
    Tir: Option<String>,
    Ons: Option<String>,
    Tor: Option<String>,
    Fre: Option<String>,
    Lor: Option<String>,
    Son: Option<String>,
    pub matches: Option<HashMap<String, bool>>,
}

impl UserUpdate{
    fn default() -> Self{
        Self{
            bio: "".to_string(),
            Man: None,
            Tir: None,
            Ons: None,
            Tor: None,
            Fre: None,
            Lor: None,
            Son: None,
            matches: None,
        }
    }
}

pub fn username_from_session(session: &Session) -> Result<String>{
    let id: String = unwrap!(
        Some(unwrap!(Ok(session.get("ID")), "UNABLE TO GET SESSION_ID")?),
        "SESSION NOT FOUND"
    )?;

    let username = unwrap!(Ok(SESSION_ID.get(id)), "UNABLE TO GET OPEN SESSION DB")?;
    Ok(String::from_utf8_lossy(&unwrap!(Some(username), "UNABLE TO FIND SESSION FOR ID")?[..]).to_string())
}

pub async fn update_user(session: Session, form: web::Form<UserUpdate>) -> Result<HttpResponse>{
    println!(" => {} : {:?}", "Updated profile".yellow().bold(), form);
    let username = username_from_session(&session)?;

    unwrap!(Ok(
        USER_DATA.insert(username, &unwrap!(Ok(toml::to_string(&form.clone())), "UNABLE TO SERIALIZE FORM DATA")?[..])
    ), "ERROR INSERTING FORM DATA INTO DB")?;

    Ok(redirect(&format!("/profile")))
}

pub fn get_user_data(username: &str) -> Result<UserUpdate>{
    let userdata = unwrap!(Ok(USER_DATA.get(username)), "FAILED TO OPEN USER DB")?;
    Ok(match toml::from_str(&str!(userdata.unwrap_or(sled::IVec::from(vec![])))){
        Ok(ok) => ok,
        Err(err) => {
            println!(" => {} deserializing user data {}", "Err".red(), err);
            UserUpdate::default()
        },
    })
}

pub async fn profile(session: Session) -> Result<HttpResponse>{
    println!("Profile");
    let username = username_from_session(&session)?;
    let userdata = get_user_data(&username)?;

    let mut page_data = mapify!(userdata, |x: Option<String>|{
        if x == Some("on".to_string()){
            "checked"
        }else{""}.to_string()
    } [Man, Tir, Ons, Tor, Fre, Lor, Son] <&str, String>);

    page_data.insert("username", username);
    page_data.insert("bio", userdata.bio);

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(parse_html("web/profile/index.html", page_data.iter().map(|(k,v)|(*k,&v[..])).collect())?))
}

pub fn find_match(username: &str) -> Result<(String, UserUpdate)>{
    let userdata = get_user_data(username)?;

    for other_user in USER_DATA.iter(){
        let other_user = unwrap!(Ok(other_user), "ERROR READING FROM USER DB")?;
        if str!(other_user.0) == username{continue}
        if unwrap!(Ok( USER_MATCHES.get(str!(other_user.0)+username)), "UNABLE TO READ USER_MATCHES")?.is_some(){continue}
        if unwrap!(Ok( USER_MATCHES.get( &format!("{}{}",username,str!(other_user.0)))), "UNABLE TO READ USER_MATCHES")?.is_some(){continue}

        let other_userdata = match toml::from_str(&str!(other_user.1)){
            Ok(ok) => ok,
            Err(err) => {
                println!(" => {} deserializing user data {}", "Err".red(), err);
                UserUpdate::default()
            },
        };

        if userdata.clone().matches.unwrap_or(HashMap::new()).get(&str!(other_user.0)).is_some(){continue}

        if compare!(
            &other_userdata,
            &userdata,
            |a,b:Option<String>|{
                &a.clone().unwrap_or("".to_string())[..]=="on" &&
                &b.clone().unwrap_or("".to_string())[..]=="on"
            }
            [Man, Tir, Ons, Tor, Fre, Lor, Son]).iter().any(|a|*a){

            return Ok((str!(other_user.0), other_userdata))
        }
    }
    Ok(("".to_string(), UserUpdate::default()))
}

pub fn index(session_id: &str) -> Result<HttpResponse>{
    let (username, userdata) = find_match(&str!(
        unwrap!(Some(unwrap!(Ok(SESSION_ID.get(session_id)), "ERROR READING USER_ID")?), "MISSING USER")?
    ))?;

    if username == ""{
        return Ok(redirect("/no_matches.html"))
    }

    let mut page_data = mapify!(userdata, |x: Option<String>|{
        if x == Some("on".to_string()){
            "checked"
        }else{""}.to_string()
    } [Man, Tir, Ons, Tor, Fre, Lor, Son] <&str, String>);

    page_data.insert("username", username.clone());
    page_data.insert("bio", userdata.bio);

    page_data.insert("image", format!("/profile_pics/{}.jpg", username));

    Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body(parse_html("web/card/index.html", page_data.iter().map(|(k,v)|(*k,&v[..])).collect())?))

}

pub async fn match_users(session: Session, req: HttpRequest) -> Result<HttpResponse>{
    println!(" => MATCH {}", ":)".green());
    let username = username_from_session(&session)?;
    let mut userdata = get_user_data(&username)?;

    let other_username = req.match_info().query("username");
    let mut other_userdata = get_user_data(other_username)?;

    if userdata.matches.is_none(){
        userdata.matches = Some(HashMap::new());
    }
    if other_userdata.matches.is_none(){
        other_userdata.matches = Some(HashMap::new());
    }

    if let Some(m) = &mut userdata.matches{
        m.insert(other_username.to_string(), true);
    }

    if let Some(m) = &mut other_userdata.matches{
        if m.get(&username).is_none(){
            m.insert(username.clone(), false);
        }
    }

    unwrap!(Ok(
            USER_DATA.insert(username, &unwrap!(Ok(toml::to_string(&userdata)),"ERROR SERIALIZING USER_DATA")?[..])
        ), "ERROR WRITING TO USER_DATA")?;

    unwrap!(Ok(
            USER_DATA.insert(other_username, &unwrap!(Ok(toml::to_string(&other_userdata)),"ERROR SERIALIZING USER_DATA")?[..])
        ), "ERROR WRITING TO USER_DATA")?;

    Ok(redirect(&format!("/{}", req.match_info().query("redirect"))))
}

pub async fn dont_match(session: Session, req: HttpRequest) -> Result<HttpResponse>{
    println!(" => NOT MATCH {}", ":(".red());
    let username = username_from_session(&session)?;
    let other_username = req.match_info().query("username");

    unwrap!(Ok(USER_MATCHES.insert(username+other_username, sled::IVec::from(vec![]))), "UNABLE TO WRITE TO USER_MATCHES")?;

    Ok(redirect("/"))
}

fn notification_card(success: bool, message: &str, sub: &str, link: &str, button: &str) -> String{
    if success{
        format!(r#"
          <div class="post-content">
            <h2>{message}</h2><p>{sub}</p>
            <a class="btn fa-comment" href="{link}"> {button}</a>
          </div><br><br>
        "#,
        message=message,
        sub=sub,
        link=link,
        button=button,
        )
    }else{
        format!(r#"
          <div class="post-content">
            <h2>{message}</h2><p>{sub}</p>
            <a class="btn fa-comment" href="{link}" style="background-color:red;"> {button}</a>
          </div><br><br>
        "#,
        message=message,
        sub=sub,
        link=link,
        button=button,
        )
    }
}

pub async fn contacts(session: Session) -> Result<HttpResponse>{
    let username = username_from_session(&session)?;
    let userdata = get_user_data(&username)?;
    let mut contacts = "".to_string();

    for (other_username, is_matched) in unwrap!(Some(userdata.matches), "NO MATCHES YET")?{
        if is_matched{
            if !get_user_data(&other_username)?.matches.unwrap_or(HashMap::new()).get(&username).unwrap_or(&false){continue}
            contacts = format!(
                r#"{}{}"#,
                contacts,
                notification_card(true, &other_username, "You have a new match", &format!("/chat/{}", other_username), "Chat"),
            );
        }else{
            contacts = format!(
                r#"{}{}"#,
                contacts,
                notification_card(false, &other_username, "Some one matched you", &format!("/match/{}/contacts", other_username), "Match back"),
            );
        }
    }


    let mut page_data = HashMap::new();
    page_data.insert("contacts", &contacts[..]);

     Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body( parse_html("web/contacts/index.html", page_data)?))
}


