use crate::{
    import::*,
    unwrap,
    anyhow,
    redirect,
    parse_html,
    SESSION_ID,
    USER_DATA,
    user::username_from_session,
    user::get_user_data,
    CONVERSATIONS,
};

pub async fn chat(session: Session, req: HttpRequest) -> Result<HttpResponse>{
    let username = username_from_session(&session)?;
    let userdata = get_user_data(&username)?;

    let other_username = req.match_info().query("username");
    let other_userdata = get_user_data(&other_username)?;

    if other_userdata.matches.unwrap_or(HashMap::new()).get(&username) != Some(&true){
        return anyhow!("ONE OR BOTH USERS ARE NOT MATCHED")
    }
    //if userdata.matches.unwrap_or(HashMap::new()).get(&other_username) != Some(&true){
    //    return anyhow!("ONE OR BOTH USERS ARE NOT MATCHED")
    //}

    let mut page_data = HashMap::new();

     Ok(HttpResponse::Ok()
        .content_type("text/html")
        .body( parse_html("web/chat.html", page_data)?))
}
