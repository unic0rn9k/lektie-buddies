var casper = require('casper').create();

casper.start('https://www.lectio.dk/lectio/518/login.aspx', function() {
    this.fill('form#contact-form', {
        'subject':    'I am watching you',
        'content':    'So be careful.',
        'civility':   'Mr',
        'name':       'Chuck Norris',
        'email':      'chuck@norris.com',
        'cc':         true,
        'attachment': '/Users/chuck/roundhousekick.doc'
    }, true);
});

casper.then(function() {
    this.evaluateOrDie(function() {
        setTimeout(function() {
            page.render('google.png');
            phantom.exit();
        }, 200);
    }, 'Failed to login');
});

casper.run(function() {
    this.echo('Logged in').exit();
});
