use colored::*;
use anyhow::*;
use regex::Regex;
use std::process::*;
use std::io::Read;
use lazy_static::*;

fn wget(o: &str, url: &str) -> Result<String>{
    let mut tmp = "".to_string();
    Command::new("wget")
        .arg("--load-cookies")
        .arg("coke.txt")
        .arg("-O")
        .arg(o)
        .arg(url)
        .stdout(Stdio::piped())
        .spawn()?.stdout.unwrap().read_to_string(&mut tmp)?;
    Ok(tmp)
}

lazy_static!{
    static ref USER_LINK: Regex = Regex::new(r"<a.*href='(?P<url>.*)'>(?P<name>.*)</a>").unwrap();
}

fn main() -> Result<()>{
    println!("\n{}", "LECTIO RIPPER".bold().red());

    for line in wget("-", "https://www.lectio.dk/lectio/518/FindSkema.aspx?type=elev&forbogstav=d")?.split("</li><li>"){
        let user = match USER_LINK.captures_iter(line).next(){
            None => continue,
            Some(ok) => ok,
        };
        println!("{} \t\t\t\t\t\t {}", &user["name"], &user["url"])
    }

    Ok(())
}
